package contenedora;

public class LocationVO<T> implements Comparable<T> {
    private int addressId;
    private String location;
    private int numberOfRegisters;
    public LocationVO(int pAddressId, String pLocation, int pNumberOfRegisters){
    	addressId = pAddressId;
    	location = pLocation;
    	numberOfRegisters = pNumberOfRegisters;
    }
    public String getLocation(){
    	return location;
    }
    public int getAddressId(){
    	return addressId;
    }
    public int getNumberOfRegisters(){
    	return numberOfRegisters;
    }
	@Override
	public int compareTo(T o) {
		// TODO Auto-generated method stub
		LocationVO variable = (LocationVO) o;
		int retorno  = 10;
		if(numberOfRegisters > variable.getNumberOfRegisters()){
			retorno = 1;
		}
		else if(numberOfRegisters < variable.getNumberOfRegisters()){
			retorno = -1;
		}
		else{
			if(location.hashCode() > variable.hashCode()){
				retorno = 1;
			}
			else if(location.hashCode() > variable.hashCode()){
				retorno = -1;
			}
			else{
				retorno = 0;
			}
		}
		return retorno;
	}

}
