package model.data_structures;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico<T extends Comparable<T>> implements IArregloDinamico<T> {
		/**
		 * Capacidad maxima del arreglo
		 */
        private int tamanoMax;
		/**
		 * Numero de elementos en el arreglo (de forma compacta desde la posicion 0)
		 */
        private int tamanoAct;
        /**
         * Arreglo de elementos de tamaNo maximo
         */
        private T elementos[ ];

        /**
         * Construir un arreglo con la capacidad maxima inicial.
         * @param max Capacidad maxima inicial
         */
		public ArregloDinamico( int max )
        {
               elementos = (T[]) new String[max];
               tamanoMax = max;
               tamanoAct = 0;
        }
		@Override
		public void agregar( T dato )
        {
               if ( tamanoAct == tamanoMax )
               {  // caso de arreglo lleno (aumentar tamaNo)
                    tamanoMax = 2 * tamanoMax;
                    T [ ] copia = elementos;
                    elementos = (T[]) new  String[tamanoMax];
                    for ( int i = 0; i < tamanoAct; i++)
                    {
                     	 elementos[i] = copia[i];
                    } 
            	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
               }	
               elementos[tamanoAct] = dato;
               tamanoAct++;
       }

		public int darTamano() {
			// TODO implementar
			return tamanoAct;
		}

		public T darElemento(int i) {
			// TODO implementar
			return elementos[i];
		}
		@Override
		public T buscar(T dato) {
			// TODO implementar
			// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
			// soluci�n �nica usando alguno de los m�todos 
			// de la clase gen�rica T : equals( ... ), toString( ) o 
			// hashCode( ) 
			// Si T no tiene el m�todo se usa el de la clase Object
			T busqueda = null;
			for(int i = 0; i < darTamano() - 1; i++){
				if(elementos[i].compareTo(dato) == 0){
					busqueda = elementos[i];
				}
			}
			return busqueda;
		}
		@Override
		public T eliminar(T dato) {
			// TODO implementar
			// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
			T datoeliminado = buscar(dato);
			T[] copycat = elementos;
			elementos = (T[]) new String[tamanoMax];
			for(int i = 0; i < tamanoAct; i++){
				elementos[i] = copycat[i];
			}
			System.out.println("Arreglo de respaldo creado");
			for(int j = 0; j < tamanoAct; j++){
				
				if(elementos[j].equals(datoeliminado)){
					elementos[j] = copycat[j+1];
					tamanoAct = tamanoAct - 1;
				}
				//Copia la lista en el copycat
				else if(elementos[j].equals(copycat[j])){
					
				elementos[j] = copycat[j];
				}
			    
			}
			return datoeliminado;
		}
		public void exch(int i, int j){
			T t = elementos[i]; elementos[i] = elementos[j]; elementos[j] = t;
		}
		/**
	     * M�todo compareTo para comparar objetos de tipo T genericos
	     * retorna 0 en caso de igualdad this y obj
	     * retorna < 0 en caso que this < obj
	     * retorna > 0 en caso que this > obj
	     */
		public int compareTo(T obj) {
			// TODO Auto-generated method stub
			T temp = (T) this;
			T temp2 = (T) obj;
			int this1 = temp.hashCode();
			int this2 = temp2.hashCode();
			int comparacion = -10;
			if( this1 == this2){
				comparacion = 0;
			}
			else if(this1 < this2){
				comparacion = -1;
			}
			else{
				comparacion = 1;
			}
			return comparacion;
		}

}
