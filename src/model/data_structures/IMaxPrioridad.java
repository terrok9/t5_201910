package model.data_structures;
/**
 * Clase interface para las estructuras de datos Heap y Cola
 * @author user Jose Fernando Barrera 201612929
 * Contiene las responsabilidades de las clases data_structures
 * @param <T> objeto g�nerico para la implementaci�n general
 */
public interface IMaxPrioridad<T>  {
/**
 * Retorna n�mero de elementos presentes en la cola de prioridad
 */ 
	public int darNumElementos();
/**
 * Agrega un elemento a la cola. 
 * Si el elemento ya existe y tiene una prioridad diferente, el elemento debe actualizarse en la cola de prioridad.
 */
	public void agregar(T elemento);
/**
 * Saca/atiende el elemento m�ximo en la cola y lo retorna; nullen caso de cola vac�a
 */
    public T delMax();
/**
 * Obtener el elemento m�ximo (sin sacarlo de la Cola); nullen caso de cola vac�a
 */
    public T max();
/**
 * Retorna si la cola est� vac�a o no
 */
    public boolean esVacia();
}
