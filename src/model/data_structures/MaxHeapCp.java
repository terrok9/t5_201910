package model.data_structures;

public class MaxHeapCp<T extends Comparable<T>>  implements IMaxPrioridad<T>, IArregloDinamico<T> {
	
	/**
	 * Arreglo de objetos
	 */
    private ArregloDinamico<T> cp;
    /**
     * Tama�o del arreglo, no se usa el 0
     */
    private int N = 0;
	public MaxHeapCp(int max){
		cp = new ArregloDinamico<T>(max);
	}
	public int darNumElementos() {
		// TODO Auto-generated method stub
		return N;
	}

	@Override
	public void agregar(T elemento) {
		// TODO Auto-generated method stub
		cp.agregar(elemento);
		swim(N);
	}

	@Override
	public T delMax() {
		// TODO Auto-generated method stub
		T max =  cp.darElemento(1);
		exch(1, N--);
		cp.eliminar(cp.darElemento(N+1));
		sink(1);
		return max;
	}

	@Override
	public T max() {
		// TODO Auto-generated method stub
	    T retorno = null;
	    retorno = cp.darElemento(1);
	    return retorno;
	}

	@Override
	public boolean esVacia() {
		// TODO Auto-generated method stub
		return N == 0;
	}
	private boolean less(int i, int j){
		return cp.darElemento(i).compareTo(cp.darElemento(j)) < 0;
	}
	private void exch(int i, int j){
		cp.exch(i, j);
	}
	private void swim(int k){
		while (k > 1 && less(k/2, k))
		{
		exch(k/2, k);
		k = k/2;
		}
	}
	private void sink(int k){
		while (2*k <= N)
		{
		int j = 2*k;
		if (j < N && less(j, j+1)) j++;
		if (!less(k, j)) break;
		exch(k, j);
		k = j;
		}
	}
	@Override
	public int darTamano() {
		// TODO Auto-generated method stub
		return cp.darTamano();
	}
	@Override
	public T darElemento(int i) {
		// TODO Auto-generated method stub
		return cp.darElemento(i);
	}
	@Override
	public T buscar(T dato) {
		// TODO Auto-generated method stub
		return cp.buscar(dato);
	}
	@Override
	public T eliminar(T dato) {
		// TODO Auto-generated method stub
		return cp.eliminar(dato);
	}
	@Override
	public int compareTo(T obj) {
		// TODO Auto-generated method stub
		return cp.compareTo(obj);
	}
}
