package model.data_structures;

public class MaxColaPrioridad<T extends Comparable<T>> implements IMaxPrioridad<T> {


	private Object[] maxColaPrioridad;

	int numElementos;
	public MaxColaPrioridad(int prioridadMax) 
	{
		maxColaPrioridad = new Object [prioridadMax];
		numElementos =0;		
	}
	public int darNumElementos() {
		// TODO Auto-generated method stub
		return numElementos;
	}

	@Override
	public void agregar(T elemento) {
		// TODO Auto-generated method stub
		maxColaPrioridad[numElementos++]=((T) elemento);

	}

	@Override
	public T delMax() {
		// TODO Auto-generated method stub
		int max = 0;
		for(int i = 0; i<numElementos; i++)
		{
			if(less(max, i))
			{
				max = i;
			}
			exch(max, numElementos-1);
		}
		return (T) maxColaPrioridad[numElementos--];
	}

	@Override
	public T max() {
		// TODO Auto-generated method stub
		int max= 0;

		if(numElementos != 0)
		{ for(int i = 0; i<numElementos; i++)
		{
			if(less(max, i))
			{
				max = i;
			}
		}
		}
		return (T) maxColaPrioridad[max];
}

	@Override
	public boolean esVacia() {
		// TODO Auto-generated method stub
		return numElementos==0;
	}
	private boolean less(int i, int j)
	{
		return ((T) maxColaPrioridad[i]).compareTo((T) maxColaPrioridad[j]) < 0;

	}
	private void exch(int i, int j)
	{
		Object t = maxColaPrioridad[i]; 
		maxColaPrioridad[i] = maxColaPrioridad[j]; 
		maxColaPrioridad[j] = t; 
	}


}
