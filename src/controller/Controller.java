package controller;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;

import com.opencsv.CSVReader;

import consola.MovingViolationsManagerView;
import contenedora.LocationVO;
import model.data_structures.IMaxPrioridad;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.MaxHeapCp;

public class Controller {
    private String[] cuatrimestre;
    
	private MovingViolationsManagerView view;
	
	/**
	 * Cola prioridad donde se van a cargar los datos de los archivos
	 */
	private IMaxPrioridad<LocationVO> movingViolationsList;
	
	/**
	 * Mont�culo donde se van a cargar los datos de los archivos
	 */
	private IMaxPrioridad<LocationVO> movingViolationsHeap;
	/**
	 * Generar una muestra
	 */
	private Comparable<LocationVO> muestra[];
	//Constructor
	public Controller() {
		view = new MovingViolationsManagerView();
		
		//TODO, inicializar la pila y la cola
		movingViolationsHeap = new MaxHeapCp(10);
		movingViolationsList = new MaxColaPrioridad(10);
		cuatrimestre = new String[4];
	}
	/**
	 * Generar una muestra aleatoria de tamaNo n de los datos leidos.
	 * Los datos de la muestra se obtienen de las infracciones guardadas en la Estructura de Datos.
	 * @param n tamaNo de la muestra, n > 0
	 * @return muestra generada
	 */
	public Comparable<LocationVO>[ ] generarMuestra( int n )
	{
		long startTime = System.currentTimeMillis();
		for (int i = 1; i < movingViolationsHeap.darNumElementos() - 1 && i <= n; i++){
			muestra[i] = movingViolationsHeap.max();
		}
		// TODO Llenar la muestra aleatoria con los datos guardados en la estructura de datos
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("muestra: " + timeTaken);
		return muestra;
		
	}
	
	
	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		Controller controller = new Controller();
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 0:
					view.printMessage("Ingrese el cuatrimestre (1, 2 o 3)");
					int numeroCuatrimestre = sc.nextInt();
					controller.loadMovingViolations(numeroCuatrimestre);
					break;
				
				case 1:
					view.printMessage("Ingrese el tama�o de la muestra (Ej: 20000)");
					int numeroMuestra = sc.nextInt();
					controller.generarMuestra(numeroMuestra);
					break;
					
				case 2:
					view.printMessage("Ingrese el addressId indicado");
					int address = sc.nextInt();
					controller.conteoAddressId(address);
					break;
					
				case 3:
					IMaxPrioridad<LocationVO> retorno3 =	controller.cronometroColaPrioridadAgregar();
			
					break;
						
					
				case 4:
					
					IMaxPrioridad<LocationVO> retorno4 = controller.cronometroHeapAgregar();
					break;
					
				case 5:
					IMaxPrioridad<LocationVO> retorno5 = controller.cronometroColaPrioridadDelMax();
					break;
				
				case 6:
					IMaxPrioridad<LocationVO> retorno6 =	controller.cronometroHeapDelMax();
					break;
					
				case 7:
				IMaxPrioridad<LocationVO> retorno7 =	controller.nViasImportantes();
					break;
					
				case 8:
					fin=true;
					sc.close();
					
					break;
					
			}
		}

}
    
	public String[] meses(int pNumeroDeCuatrimestre){
		if(pNumeroDeCuatrimestre == 1){
			cuatrimestre[0] = "January";
			cuatrimestre[1] = "February";
			cuatrimestre[2] = "March";
			cuatrimestre[3] = "April";
		}
		else if(pNumeroDeCuatrimestre == 2){
        	cuatrimestre[0] = "May";
        	cuatrimestre[1] = "June";
        	cuatrimestre[2] = "July";
        	cuatrimestre[3] = "August";
		}
        else{
        	cuatrimestre[0] = "September";
        	cuatrimestre[1] = "October";
        	cuatrimestre[2] = "November";
        	cuatrimestre[3] = "December";
        }
		return cuatrimestre;
	}

	public void loadMovingViolations(int pNumeroDeCuatrimestre) 
	{
		// TODO
		cuatrimestre = meses(pNumeroDeCuatrimestre);
		try
		{
			 CSVReader reader = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[0]+ "_2018.csv" ));
			 CSVReader reader2 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[1]+ "_2018.csv"));
			 CSVReader reader3 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[2]+ "_2018.csv"));
			 CSVReader reader4 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[3]+ "_2018.csv"));
      String[] nextLine;
      reader.readNext();
      while((nextLine = reader.readNext()) != null)
      {
    	
    	LocationVO a = new LocationVO(Integer.parseInt(nextLine[2]), nextLine[3], 0);
    	movingViolationsHeap.agregar(a);
    	
    	
      }
      String[] nextLine2;
      reader2.readNext();
      while((nextLine2 = reader2.readNext()) != null)
      {
    	  LocationVO a = new LocationVO(Integer.parseInt(nextLine2[2]), nextLine2[3], 0);
      	movingViolationsHeap.agregar(a);
    	
    	
      }
      String[] nextLine3;
      reader3.readNext();
      while((nextLine3 = reader3.readNext()) != null)
      {
    	
    	  LocationVO a = new LocationVO(Integer.parseInt(nextLine3[2]), nextLine3[3], 0);
      	movingViolationsHeap.agregar(a);
    	
      }
      String[] nextLine4;
      reader4.readNext();
      while((nextLine4 = reader4.readNext()) != null)
      {
    	LocationVO a = new LocationVO(Integer.parseInt(nextLine4[2]), nextLine4[3], 0);
      	movingViolationsHeap.agregar(a);
    	
      }
      reader.close();
      reader2.close();
      reader3.close();
      reader4.close();
		}
    catch(Exception e)
    {
    	e.getMessage();
    }
	}
	public Comparable<LocationVO>[] conteoAddressId(int address){
		for(int i = 0; i < muestra.length; i++){
		LocationVO a = (LocationVO) muestra[i];
		if(a.getAddressId() != address){
		  muestra[i] = null;
		}
		}
		return muestra;
	}
	public IMaxPrioridad<LocationVO> cronometroHeapAgregar(){
		IMaxPrioridad<LocationVO> retorno = new MaxHeapCp(10);
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < muestra.length; i++){
			retorno.agregar((LocationVO) muestra[i]);
		}
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("Time taken: " + timeTaken);
		return retorno;
	}
	public IMaxPrioridad<LocationVO> cronometroHeapDelMax(){
		long startTime = System.currentTimeMillis();
		IMaxPrioridad<LocationVO> arreglo = cronometroHeapAgregar();
		MaxHeapCp retorno = (MaxHeapCp) arreglo;  
		for (int i = 0; i < retorno.darNumElementos(); i++){
			retorno.delMax();
		}
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("Time taken: " + timeTaken);
		return retorno;
	}
	public IMaxPrioridad<LocationVO> nViasImportantes(){
		IMaxPrioridad<LocationVO> retorno = new MaxHeapCp(10);
		for(int i = 0; i < movingViolationsHeap.darNumElementos(); i++){
			LocationVO a = movingViolationsHeap.delMax();
			for(int j = i; j < retorno.darNumElementos(); j++){
				if(a.compareTo(retorno.max()) == 1){
					retorno.agregar(a);
				}
				else if(a.compareTo(retorno.max()) == -1){
					retorno.delMax();
					retorno.agregar(a);
				}
				else{
					retorno.agregar(a);
					retorno.delMax();
				}
			}
			
		
		}
		return retorno;
		
	}
	 public IMaxPrioridad<LocationVO> cronometroColaPrioridadAgregar()
	{
		IMaxPrioridad<LocationVO> retorno = new MaxColaPrioridad(10);
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < muestra.length; i++){
			retorno.agregar((LocationVO) muestra[i]);
		}
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("Time taken: " + timeTaken);
		return retorno;
	}
	
	public IMaxPrioridad<LocationVO> cronometroColaPrioridadDelMax(){
		long startTime = System.currentTimeMillis();
		IMaxPrioridad<LocationVO> arreglo = cronometroColaPrioridadAgregar();
		MaxColaPrioridad retorno = (MaxColaPrioridad) arreglo;  
		for (int i = 0; i < retorno.darNumElementos(); i++){
			retorno.delMax();
		}
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("Time taken: " + timeTaken);
		return retorno;
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	 private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)

     {

                    return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));

     }
}
