package consola;



import contenedora.LocationVO;
import model.data_structures.IMaxPrioridad;
import model.data_structures.MaxHeapCp;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller5----------------------");
		System.out.println("0. Cargar datos del cuatrimestre");
		System.out.println("1. Generar muestra aleatoria de infracciones de tamaño N (N es un dato de entrada)");
		System.out.println("2. Conteo de infracciones en la muestra con el mismo AddressId en objetos LocationVO.");
		System.out.println("3. Cronometrar el tiempo de aplicar el método agregar(…) a una MaxColaPrioridad los LocationVOs del subconjunto resultante.");
		System.out.println("4. Realizar el mismo paso anterior pero agregando el subconjunto de LocationVOs en una MaxHeap");

		
		System.out.println("5. Cronometrar el tiempo de aplicar el método delMax(…) a todos los elementos LocationVOs presentes en la MaxColaPrioridad.");
		System.out.println("6. Realizar el mismo paso anterior sacando todos los elementos del MaxHeap.");
		System.out.println("7. Mostrar la N vías (Location y AddressId) mas importantes por su números de accidentes (de mayor a menor).");
		
		System.out.println("8. Salir");
		System.out.println("Digite el nï¿½mero de opciï¿½n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}
	
	public void printMovingViolationsReq2(Comparable<LocationVO>[] resultados2) {
		for(int i = 0; i < resultados2.length; i++) {
			LocationVO v = (LocationVO) resultados2[i];
			System.out.println(v.getAddressId() + " " + v.getLocation());
		}
	}
	
	public void printMovingViolationsReq4(MaxHeapCp resultados2) {
		for(int i = 0; i < resultados2.darNumElementos(); i++) {
			LocationVO v = (LocationVO) resultados2.delMax();
			System.out.println(v.getAddressId() + " " + v.getLocation());
		}
	}
	public void printMovingViolationsReq6(MaxHeapCp resultados2) {
		for(int i = 0; i < resultados2.darNumElementos(); i++) {
			LocationVO v = (LocationVO) resultados2.delMax();
			System.out.println(v.getAddressId() + " " + v.getLocation());
		}
	}
	public void printMovingViolationsReq7(MaxHeapCp resultados2) {
		for(int i = 0; i < resultados2.darNumElementos(); i++) {
			LocationVO v = (LocationVO) resultados2.delMax();
			System.out.println(v.getAddressId() + " " + v.getLocation());
		}
	}
}

