package model.data_structures;

import contenedora.LocationVO;
import junit.framework.TestCase;

public class MaxHeapCpTest extends TestCase{
	
	private MaxHeapCp<pruebaVO> heap;
	
	void setupEscenario1(){
		heap = new MaxHeapCp<pruebaVO>(10);
	}
    void setupEscenario2(){
    	heap = new MaxHeapCp<pruebaVO>(3);
    	pruebaVO prueba1= new pruebaVO(90210, "B", 1);
		pruebaVO prueba2= new pruebaVO(90211, "C", 2);
		pruebaVO prueba3= new pruebaVO(90212, "E", 3);
		heap.agregar(prueba1);
		heap.agregar(prueba2);
		heap.agregar(prueba3);
    }
    void testDarNumeroElementos() 
	{
		setupEscenario1();
		assertEquals(0, heap.darNumElementos());
		try
		{
			setupEscenario2();
			assertEquals(3, heap.darNumElementos());
			setupEscenario2();
			assertEquals(3, heap.darNumElementos());

		}
		catch(Exception e)
		{
			fail("No deberia generar excepcion");
		}
	}

	
	void testAgregar() 
	{
		try
		{
			setupEscenario1();
			heap.agregar(new pruebaVO(90201,"A", 10));
			assertEquals(1, heap.darNumElementos());
			
		}
		catch(Exception e)
		{
			fail("No deberia generar excepcion");
		}

	}
}
