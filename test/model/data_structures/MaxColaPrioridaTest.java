package model.data_structures;


import junit.framework.TestCase;
import model.data_structures.MaxColaPrioridad;

class MaxColaPrioridadTest extends TestCase
{
	//-----------------------------------------------------------
	// Attributes
	//-----------------------------------------------------------

	private MaxColaPrioridad<pruebaVO> list;

	//-----------------------------------------------------------
	// Scenarios
	//-----------------------------------------------------------

	void setupScenario0()
	{
		list = new MaxColaPrioridad<>(3);
	}
	

	void setupScenario1() 
	{
		list = new MaxColaPrioridad<>(3);

		pruebaVO prueba1= new pruebaVO(90210, "B", 1);
		pruebaVO prueba2= new pruebaVO(90211, "C", 2);
		pruebaVO prueba3= new pruebaVO(90212, "E", 3);

		list.agregar(prueba1);
		list.agregar(prueba2);
		list.agregar(prueba3);
	}
	void setupScenario2() 
	{
		list = new MaxColaPrioridad<>(3);

		pruebaVO prueba1= new pruebaVO(01234, "D", 3);
		pruebaVO prueba2= new pruebaVO(43210, "B", 2);
		pruebaVO prueba3= new pruebaVO(23140, "C", 1);

		list.agregar(prueba1);
		list.agregar(prueba2);
		list.agregar(prueba3);
	}


	
	void testDarNumeroElementos() 
	{
		setupScenario0();
		assertEquals(0, list.darNumElementos());
		try
		{
			setupScenario2();
			assertEquals(3, list.darNumElementos());
			setupScenario2();
			assertEquals(3, list.darNumElementos());

		}
		catch(Exception e)
		{
			fail("No deberia generar excepcion");
		}
	}

	
	void testAgregar() 
	{
		try
		{
			setupScenario0();
			list.agregar(new pruebaVO(90201,"A", 10));
			assertEquals(1, list.darNumElementos());
			
		}
		catch(Exception e)
		{
			fail("No deberia generar excepcion");
		}

	}
}
